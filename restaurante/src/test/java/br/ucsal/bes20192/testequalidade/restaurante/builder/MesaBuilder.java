package br.ucsal.bes20192.testequalidade.restaurante.builder;

import br.ucsal.bes20192.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20192.testequalidade.restaurante.enums.SituacaoMesaEnum;

public class MesaBuilder {
	
	public static final Integer NUMERO_DEFAULT = 1;
	public static final Integer CAPACIDADE_DEFAULT = 1;
	public static final SituacaoMesaEnum SITUACAO_DEFAULT = null;
	
	private Integer numero = NUMERO_DEFAULT;
	private Integer capacidade = CAPACIDADE_DEFAULT; 
	private SituacaoMesaEnum situacao = SITUACAO_DEFAULT;
	
	private MesaBuilder() {
	}
	
	public static MesaBuilder umaMesa() {
		return new MesaBuilder();
	}
	
	public static MesaBuilder umaMesaLivre() {
		return MesaBuilder.umaMesa()
				.deNumero(1)
				.comCapacidade(5)
				.comSituacaoLivre();
	}
	
	public MesaBuilder deNumero(Integer numero) {
		this.numero = numero;
		return this;
	}
	
	public MesaBuilder comCapacidade(Integer capacidade) {
		this.capacidade = capacidade;
		return this;
	}
	
	public MesaBuilder comSituacao(SituacaoMesaEnum situacao)  {
		this.situacao = situacao;
		return this;
	}
	
	public MesaBuilder comSituacaoLivre()  {
		this.situacao = SituacaoMesaEnum.LIVRE;
		return this;
	}
	
	public MesaBuilder comSituacaoOcupada()  {
		this.situacao = SituacaoMesaEnum.OCUPADA;
		return this;
	}
	
	public MesaBuilder mas() {
		return MesaBuilder
				.umaMesa()
				.deNumero(numero)
				.comCapacidade(capacidade)
				.comSituacao(situacao);
	}
	
	public Mesa build() {
		Mesa mesa = new Mesa(numero);
		mesa.setCapacidade(capacidade);
		mesa.setSituacao(situacao);
		return mesa;
	}
	
}
