package br.ucsal.bes20192.testequalidade.restaurante.domain;

import br.ucsal.bes20192.testequalidade.restaurante.enums.SituacaoMesaEnum;

public class Mesa {

	private SituacaoMesaEnum situacao = SituacaoMesaEnum.LIVRE;

	private Integer numero;

	private Integer capacidade;

	public Mesa(Integer numero) {
		super();
		this.numero = numero;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public SituacaoMesaEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoMesaEnum situacao) {
		this.situacao = situacao;
	}

	public Integer getCapacidade() {
		return capacidade;
	}

	public void setCapacidade(Integer capacidade) {
		this.capacidade = capacidade;
	}

	@Override
	public String toString() {
		return "Mesa [situacao=" + situacao + ", numero=" + numero + ", capacidade=" + capacidade + "]";
	}

}
